## Inspiration
We wanted to make this hack a hardware one and a user attendance app seemed to fit well. It is useful and relevant, and combines various technologies.

## How it works
It works on a client server model.
Users are clients that come in proximity of a region pinpointed by its latitude and longitude. Their mobile devices transmit the GPS location to the server, which then checks which groups they are a part of and depending on the location, marks them present or not.

## Challenges I ran into
Public IP: For the server to be accessible from anywhere, the IP had to be public.

## Accomplishments that I'm proud of
Designed the server in an easily extensible manner

## What I learned
A bit of the interfacing required between a mobile device and a static server

## What's next for Smart Attendance System
Better region measurement
More features like answer polling